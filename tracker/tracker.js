// Storage coordiantes
const StorageCtrl = (function(){

	return{
		clearCoordinatesFromStorage: function(){
			localStorage.removeItem('coordinates');
		},

		storeCoordinates: function(coordinate){
			
			let coordinates;

			// Check if  any item in LS
			if (localStorage.getItem('coordinates')=== null) {

				coordinates = [];

				// push new item
				coordinates.push(coordinate);

				// Set LS
				localStorage.setItem('coordinates', JSON.stringify(coordinates));

			}else{
				coordinates = JSON.parse(localStorage.getItem('coordinates'));

				// Push new item
				coordinates.push(coordinate);

				// Re set LS
				localStorage.setItem('coordinates', JSON.stringify(coordinates));
			}

		},

		getCoordinateFromStorage: function(coordinate){
			let coordinates;


			if (localStorage.getItem('coordinates') === null) { 

				coordinates = [];

			}else{
				coordinates = JSON.parse(localStorage.getItem('coordinates'));
			}

			return coordinates;

		}
	}
})();

// UI Controller
const UICtrl = (function(){

	const UISelectors = {
		body: 'body',
		x: '#coordinate-x',
		y: '#coordinate-y',
		itemList: '#item-list',
		addBtn: '.add-btn',
		// updateBtn: '.update-btn',
		// deleteBtn: '.delete-btn',
		// backBtn: '.back-btn',
		clearBtn: '.clear-btn'
		
	}



	// Public
	return {
		
		getSelectors: function(){
			return UISelectors;
		},

		addListCoordinate: function(coordinate){

			

			// Create li element
			const li = document.createElement('li');
			// Add class
			li.className = 'collection-item';
			// Add HTML
			li.innerHTML = `<strong>X: </strong>${coordinate[0]} - <strong>Y: </strong>${coordinate[1]}`;

			// Insert list coordinate
			document.querySelector(UISelectors.itemList).insertAdjacentElement('beforeend',li);
		}
	}

})();

// Coordiante Controller
const CoordinateCtrl = (function(){

	// Item constructor
	const Coordinate = function(x,y){
		this.x = x;
		this.y = y;
		
	} 

	// Data Structure / State
	const data = {
		// cordinates: [
		// 	{x: 100, y: 1000},
		// 	{x: 100, y: 995},
		// 	{x: 100, y: 990},

		// ],
		// cordinates: StorageCtrl.getCoordinateFromStorage(),
		currentItem: null,
		totalCalories: 0
	}

	return{

		getItems: function(){
			return data.items;
		},

		

		logData: function(){
			return data;
		}
	}
})();





// App Controller
const App = (function(ItemCtrl, StorageCtrl, UICtrl){

	// Load event listeners
	const loadEventListeners = function(){

		// Get UI Selectors
		const UISelectors = UICtrl.getSelectors();

		const body = document.querySelector(UISelectors.body);
		

		// Show Coordinates
		document.querySelector(UISelectors.addBtn).addEventListener('click', showCoordinates);

		// Get coordinates X and Y
		body.addEventListener('mousemove', getCoordinates);

		// Clear coordinates event
		document.querySelector(UISelectors.clearBtn).addEventListener('click', clearAllItems);

		// Disable submit on enter
		// document.addEventListener('keypress', function(e){
		// 	if (e.keycode ===13 || e.which === 13) {
		// 		e.preventDefault();
		// 	}
		// });

	}

	// Show coordinates
	function showCoordinates(e){

		
		const coordinates = StorageCtrl.getCoordinateFromStorage();
		coordinates.forEach( function(coordinate, index) {
			console.log(coordinate); 
			UICtrl.addListCoordinate(coordinate);
		});
		// console.log(coordinates); 
		e.preventDefault();
	}

	// Clear items event
	const clearAllItems = function(){
		

		// Remove from UI
		// UICtrl.removeCoordinates();

		// Clear from LS
		StorageCtrl.clearCoordinatesFromStorage();
	}

	

	// Event Handler
	function getCoordinates(e){

		// Get UI Selectors
		const UISelectors = UICtrl.getSelectors();

		let x = document.querySelector(UISelectors.x);
		let y = document.querySelector(UISelectors.y);

		x.value = e.offsetX;
		y.value = e.offsetY;

		const newCoordinate = [x.value, y.value];

		StorageCtrl.storeCoordinates(newCoordinate);

	}

	return {
		init: function(){
			console.log("Initializing"); 

			loadEventListeners();
		}
	}

})(CoordinateCtrl, StorageCtrl, UICtrl);

App.init();