
// Declaration
cardAction = document.querySelector('.card-action');
ul = document.querySelector("ul.collection");
oldHeading = document.getElementById('task-title');
clearBtn = document.querySelector('.clear-tasks');
addBtn = document.getElementById('add-task');
inputTask = document.getElementById('task');
inputFilter = document.getElementById('filter');

var i = 0;

// Adding li to ul
li = document.createElement("li");
li.className = "collection-item";
text = document.createTextNode("hola");
li.appendChild(text);
li.setAttribute('title', 'new-item');
li.classList.add('newClass');
// li.classList.remove('newClass');

const link = document.createElement('a');
link.className = 'delete-item secondary-content';
link.innerHTML = '<i class= "fa fa-remove"></i>';
// link.getAttribute('href');
// link.setAttribute('href', "google.com");
// link.hasAttribute('href');
// link.removeAttribute('href');
li.appendChild(link);

ul.appendChild(li);

// Replacing the header
newHeading = document.createElement('h5');
newText = document.createTextNode("Old Tasks");
newHeading.appendChild(newText);

cardAction.replaceChild(newHeading, oldHeading);

clearBtn.addEventListener('click', clearTasks);
ul.addEventListener('click', removeLi);
addBtn.addEventListener('click', addTask);
inputFilter.addEventListener('keyup', filterLi);


// Filtering results
function filterLi(e){
	filterText = inputFilter.value;
	allLi = document.querySelectorAll('li');
	var i = 0;

	if (filterText.value =='') {

		showAll();

	}else{
	
		pattern = new RegExp(filterText);
		allLi.forEach(function(item){

			var bol = pattern.test(item.textContent);

			if (!bol) {
				item.style.display = "none";
			}else{
				item.style.display = "block";
 			}
		});
	}
	console.log(filterText);
	
	

	

	// console.log(allLi); 
	e.preventDefault();
}

// Remove a LI
function removeLi(e){
	console.log(e.target.parentElement.innerText); 

	if (e.target.classList.contains('fa-remove')) {
		e.target.parentElement.parentElement.remove();
	}


}

// Clear Tasks
function clearTasks(e){

	con = confirm("Are you sure you want to clean everything up?")

	if (con) {
		liAll = document.querySelectorAll('li');

		liAll.forEach(function(liItem){
			liItem.remove();
		});

		console.log(liAll); 
	}else {
		console.log("Action cancelled");
	}

	

	e.preventDefault();

}
// Add Task
function addTask(e){

	showAll();

	li = document.createElement("li");
	li.className = "collection-item";
	text = document.createTextNode(inputTask.value);
	li.appendChild(text);

	const link = document.createElement('a');
	link.className = 'delete-item secondary-content';
	link.innerHTML = '<i class= "fa fa-remove"></i>';
 	li.appendChild(link);

 	inputTask.value = '';

	ul.appendChild(li);
	e.preventDefault();

}

function showAll(){

	inputFilter.value = '';
	allLi = document.querySelectorAll('li');
	allLi.forEach(function(item){
			item.style.display = "block";
	});
}


// console.log(ul);