// Game Values
let min = 1,
	max = 10,
	winningNum = getRandomNum(min,max),
	guessesLeft = 3;

// UI Elements
const 	game = document.querySelector('#game'),
		minNum = document.querySelector('.min-num'),
		maxNum = document.querySelector('.max-num'),
		guessBtn = document.querySelector('#guess-btn'),
		guessInput = document.querySelector('#guess-input'),
		message = document.querySelector('.message');

// Assign UI min and max
minNum.textContent = min;
maxNum.textContent = max;


// Listen for guess
guessBtn.addEventListener('click', function(){

	let guess = parseInt(guessInput.value);

	if (guessesLeft==0) {
		guessBtn.value = 'Submit';
		guessInput.disabled = 'false';
		guessesLeft = 3;
	}

	if (isNaN(guess) || guess < min || guess > max) {

		setMessage(`Please enter a number between ${min} and ${max} `, 'red');
	}else{

		if (guess == winningNum) {
			guessInput.disabled = true;
			gameOver('You guessed the number. Congrats¡', true);
		}else{
			guessesLeft -=1;
			if (guessesLeft == 0) {
				setMessage(`Game Over. The number was ${winningNum}`);
				guessInput.disabled = 'true';
				guessBtn.value = 'Play Again';
			}else{
				guessInput.value = '';
				setMessage(`Wrong number. You have ${guessesLeft} tries`, 'red');
			}
		}

	}
});

function getRandomNum(min, max){

	return Math.floor((Math.random()*(max-min+1)+min));

}

function gameOver(msg,won){

	let color;
	won === true ? color = 'green' : color = 'red';

	// Disable input
	guessInput.disabled = 'true';

	// Change border color
	guessInput.style.borderColor = color;
	guessInput.style.textColor = color;

	// Set Message
	setMessage(msg, color);

}

function setMessage(msg, color){
	message.style.color = color;
	message.textContent = msg;
}