<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Book List</title>
    <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.css" />

<style>
  .success, .error{
    color: white;
    padding: 5px;
    margin: 5px 0 15px 0;
  }
  .success{
    background: green;
  }
  .error{
    background: red;
  }


</style>
 
</head>
<body>

  <div class="container">
    <h1> Add Book</h1>
    <form id="book-form" >
      <div>
        <label for="title">Title</label>
        <input type="text" id="title" class="u-full-width">
      </div>
      <div>
        <label for="author">Author</label>
        <input type="text" id="author" class="u-full-width">
      </div>
      <div>
        <label for="isbn">ISBN</label>
        <input type="text" id="isbn" class="u-full-width">
      </div>

      <div><input type="submit" value="Submit" class="u-full-width"></div>


    </form>

    <hr>

    <table class="u-full-width">
      <thead>
        <tr>
          <th>Title</th>
          <th>Author</th>
          <th>ISBN</th>
          <th></th>
        </tr>
      </thead>

      <tbody id="book-list"></tbody>
    </table>

</div>



  <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
   <!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
  <!-- <script src="book.js"></script> -->
  <script src="book6.js"></script>
</body>
</html>