class UI{

	constructor(){

		// Get profile input
		this.profile = document.getElementById('profile');

		
	}

	showRepos(repos){

		let output = '';

		repos.forEach(function(repo){

			// console.log(repo); 

			output +=`
<div class="card card-body mb-2">	
	<div class="row">
		<div class="col-md-6">
			<a href="${repo.html_url}" target="_blank">${repo.name}</a>
		</div>
		<div class="col-md-6">

			<span class="badge badge-primary mb-2">Stars: ${repo.stargazers_count}</span>
			<span class="badge badge-secondary mb-2">Public Gists: ${repo.watchers_count}</span>
			<span class="badge badge-success mb-2"> Followers: ${repo.forms_count}</span>
			
		</div>
	</div>
</div>
			 `;

		});

		// Output repos
		document.getElementById('repos').innerHTML = output;
		
	}
	

	showProfile(user){

		this.profile.innerHTML = `
			<div class="card card-body mb-3">
				<div class="row">
					<div class="col-md-3">
						<img src="${user.avatar_url}" alt="" class="img-fluid mb-2" />
						<a href="${user.html_url}" target="_blank" class="btn btn-primary btn-block mb-2">View Profile</a>
					</div>

					<div class="col-md-9">
						<span class="badge badge-primary mb-2">Public Repos: ${user.public_repos}</span>
						<span class="badge badge-secondary mb-2">Public Gists: ${user.public_repos}</span>
						<span class="badge badge-success mb-2"> Followers: ${user.followers}</span>
						<span class="badge badge-info mb-2">Following: ${user.following}</span>
						<br><br>
						<ul class="list-group">
							<li class="list-group-item mb-2">Company: ${user.company}</li>
							<li class="list-group-item mb-2">Websit/Blog: ${user.blog}</li>
							<li class="list-group-item mb-2">Location: ${user.location}</li>
							<li class="list-group-item mb-2">Member Since: ${user.created_at}</li>
						</ul>
					</div>
				</div>

			</div>
			<h3 class="page-heading mb-3">Lates Repos</h3>
			<div id="repos"></div>

		`;

	}

	clearProfile(){
		this.profile.innerHTML= "";
	}

	showAlert(msg, className){

		// Clear any alerts
		this.clearAlert();

		// Create div
		const div = document.createElement('div');
		div.className = className;

		// Add text
		div.appendChild(document.createTextNode(msg));
		// Get parent
		const container = document.querySelector('.searchContainer'); 

		// Get search box
		const search = document.querySelector('.search');

		// Inser Alert
		container.insertBefore(div, search);

		// Timeout alert
		setTimeout(() => {
			this.clearAlert();
		},3000);

	}

	clearAlert(){
		// get alert
		const currentAlert = document.querySelector('.alert');

		if(currentAlert){
			currentAlert.remove();
		}
	}




}//END CLASS