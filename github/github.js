class Github{

	constructor(){
		this.client_id = '99c40a35d81a903f9b9c';
		this.client_secret = 'cf46c9f4ae7aa90133ffa2979bbcf173bea26e59';
		this.repos_count = 10;
		this.repos_sort = 'create: asc';
	}

	async getUser(user){


		const profileResponse = await fetch(`https://api.github.com/users/${user}?client_id=${this.client_id}?client_secret=${this.client_secret}`);
		// const profileResponse = await fetch(`https://api.github.com/users/mgm2030?client_id=99c40a35d81a903f9b9c?client_secret=cf46c9f4ae7aa90133ffa2979bbcf173bea26e59`);

		// get reports
		const reposResponse = await fetch(`https://api.github.com/users/${user}/repos?per_page=${this.repos_count}&sort=${this.repos_sort}&client_id=${this.client_id}?client_secret=${this.client_secret}`);
		// const reposResponse = await fetch(`https://api.github.com/users/mgm2030/repos?per_page=10&sort=create: asc&client_id=99c40a35d81a903f9b9c?client_secret=cf46c9f4ae7aa90133ffa2979bbcf173bea26e59`);

 		const profile = await profileResponse.json();
 		const repos = await reposResponse.json();


		return{
			// profile:profile
			profile,
			repos
		}
		
	}




} //END CLASS
