<?php
//TODO
//PROPERTIES OF TABLE HAVE TO BE SET AUTOMATICALLY
//the connections
class Base {
	
	var $conn;
	var $filter = array();
	var $numRows;
	var $select;
	
	var $columns;
	var $values;
	var $columnsValue = array();
	var $updateString;
	var $columnsTable;
	var $from;
	var $where;
	var $groupBy;
	var $orderBy;
	var $limit;

	function __construct($table, $columns='*'){

		$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
		if ($mysqli->connect_errno) {
			echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
		}
		// echo DB_HOST . ':' . DB_PORT,DB_USER,DB_PASSWORD,DB_NAME;
		$this->conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
		// $this->conn = new mysqli("localhost:5432", "root", "pass", "mgm");
		// Check connection
		if (mysqli_connect_errno())
		{
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
		
		$this->columns = $columns;
		
		$this->from = $table;
		$this->where = '1';
		// $this->groupBy = $groupBy;
		// $this->orderBy = $orderBy;
		$this->limit = '';
		$this->getColumnsName();

	}

	function getColumnsName(){
		$sql = "SHOW COLUMNS FROM `$this->from`";

		// echo $sql . "<br>";

		$list = $this->execute($sql, TRUE);
		
		foreach ($list as $key => $value) {
			$this->columnsTable[$value['Field']] = $value['Field'];
		}
		
	}

	function removeTrash($fields=NULL){
		if ($fields) {
			unset($fields['conn']);
			unset($fields['filter']);
			unset($fields['numRows']);
			unset($fields['select']);
			unset($fields['columns']);
			unset($fields['from']);
			unset($fields['where']);
			unset($fields['groupBy']);
			unset($fields['orderBy']);
			unset($fields['limit']);
			unset($fields['values']);
			unset($fields['columnsValue']);
			unset($fields['updateString']);
			unset($fields['columnsTable']);
			return $fields;
		}else{
			unset($this->conn);
			unset($this->filter);
			unset($this->numRows);
			unset($this->select);
			unset($this->columns);
			unset($this->from);
			unset($this->where);
			unset($this->groupBy);
			unset($this->orderBy);
			unset($this->limit);
			unset($this->values);
			unset($this->columnsValue);
			unset($this->updateString);
			unset($this->columnsTable);
		}
		
	}
	function getProperties(){

		$fields = get_object_vars($this);

		$fields = $this->removeTrash($fields);
		//Reseting columns (by default *)
		$this->columns ='';

		
		
		
		$numArray = count($fields);
		// STORING THE COLUMNS / VALUE IN DIFFERENT WAYS
		foreach ($fields as $k => $v) {
			// IF ID IS EMPTY... SET UP AS NULL FOR INSERT QUERY
			if ($k == 'id') {
				if ($v == '') {
					$v = 'NULL';
				}
			}
			
			// STORING COLUMNS AND VALUES AS STRING IN ITS OWN PROPERTY
			$this->columns .= "`" . $k . "`";
			$this->values  .= "'" . $v . "'";
			// STORING COLUMNS AND VALUES AS AN ARRAY
			$this->columnsValue[$k] = $v;
			// STORING COLUMNS AND VALUES TOGETHER AS STRING
			$this->updateString .= "`" . $k . "` = '" . $v ."'";
		
			// ADDING ',' TO CONSTRUCT A PROPER STRING
			if (0 === --$numArray) {
				
			}else{
				$this->columns .= ', ';
				$this->values  .= ', ';
				$this->updateString .= ', ';
			}
			
		}
	
	}

	function select(){
		
		if (isset($this->filter)) {
			foreach ($this->filter as $k => $v) {
				$this->where .= ' AND ' . $v;
			}
		}
		$sql = "SELECT {$this->columns} FROM  {$this->from} WHERE {$this->where} {$this->groupBy} {$this->orderBy} {$this->limit}";
				
		return $sql;
	}
	
	function setProperties($fields){	
		

		if ($fields) {
			//SETTING COLUMNS AS PROPERTIES OF THE OBJECT
			foreach ($fields as $k => $v) {

				if (in_array($k, $this->columnsTable)) {
					$this->$k = $v;
				}
				
			}
			
		}
		
	}

	function listItems($fields=null){

		if ($fields) {
			$this->columns = $fields;
		}
	
		$sql = $this->select();
	 
		$list = $this->execute($sql);		
	
		return $list;
	}

	function countItems(){
		$this->columns = "COUNT(`id`)";
		$sql = $this->select();
		 $list = $this->execute($sql);
		return $list[0]['COUNT(`id`)'];
	}
	// This function give all the values no repeated of one column
	function getList($column){
		$this->columns = $column;
        $this->groupBy = "GROUP BY " . $column;
        $sql = $this->select();
		 
		$list = $this->execute($sql,TRUE,TRUE);
		return $list;
	}
	function load($id){
		$this->filter[] = "id = " . $id;
		
		$sql = $this->select();
		
		$list = $this->execute($sql);
		
		return $list[0];
	
	}
	//SAVE ITEM OR UPDATE
	function save($id = 'id'){
		$this->getProperties();
		
		if (!$this->id) {
			$sql = "INSERT INTO `$this->from` ($this->columns) VALUES ($this->values);";
			
			$this->execute($sql,FALSE);
			
			$message['msg'] = "Saved correctly";
		
		}else{
		
			$sql = "UPDATE `$this->from` SET $this->updateString WHERE `$this->from`.`id` = $this->id;";
			
			$this->execute($sql,FALSE);
			
			$message['msg'] = "Updated correctly";
		}
	}
	// EXECUTING SQL QUERY. FETCH = FALSE WHEN IT IS NOT REQUIRED A RETURN (SAVE, UPDATE). 
	function execute($sql,$fetch=TRUE,$groupBy=FALSE){
		// echo $sql ."<br>";
		
		$res = mysqli_query($this->conn,$sql);
		if (!$res) {
			if (DEBUG) {
				echo 'MySQL Error: ' . mysqli_error();
			}
		  	$message['error'] = "Error. Please contact to the administrator";
		  	return $message;
		}
		
		// $this->removeTrash();
		if ($fetch) {
			if ($res->num_rows) {
			 $this->numRows = $res->num_rows;
			}
			
			if (mysqli_num_rows($res) > 0) {
				if (!$groupBy) {
					while ($r = mysqli_fetch_assoc($res)) {
					    
					        $list[] = $r;
					 }
				
				}else{
					while ($r = mysqli_fetch_assoc($res)) {
					    
					    foreach ($r as $k => $v) {
					    
					 		$list[$k][] = $v;
					    }
					        
					}
					    
					return $list[$k];
				}
				    
				return $list;
			
			}
		}
		return $res;
	}
	
	function message($message){
		if (isset($message['error'])) {
		echo "<div class='alert alert-danger'>" . $message['error'] . "</div>";
		}
		if (isset($message['msg'])) {
		echo "<div class='alert alert-success'>" . $message['msg'] . "</div>";
		}
	}
	
}
		?>