const PageState = function(){

	let currentState = new homeState(this);

	this.init = function(){
		this.change(new aboutState);
	}



	this.change = function(state){
		currentState = state;	
	}
};


// Home State
const homeState = function(page){
	// Comment
	 document.querySelector('#heading').textContent = null;
	 document.querySelector('#content').innerHTML = `
		<div class="jumbotron">
		  <h1 class="display-3">Hello, world!</h1>
		  <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
		  <hr class="my-4">
		  <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
		  <p class="lead">
		    <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
		  </p>
		</div>

	 `;
};

// About State
const aboutState = function (page){

	document.querySelector('#heading').textContent = 'About us';
	document.querySelector('#content').innerHTML = `
	<p>This is the about page</p>
	`;
}

// About State
const contactState = function (page){

	document.querySelector('#heading').textContent = 'Contact us';
	document.querySelector('#content').innerHTML = `
	<form action="">
		<div class="form-group">
			<label for="">Name</label>
			<input type="text" name="" id="" class="form-control" />
		</div>
		<div class="form-group">
			<label for="">Password</label>
			<input type="password" name="" id="" class="form-control" />
		</div>
		<button type="submit" class="btn btn-primary">Submit</button>

	</form>
	`;
};

// Instantiate pageState

const page = new PageState;
page.init();

// UI Vars
// Home
const home = document.getElementById('home');
// About
const about = document.getElementById('about');
// Contact
const contact = document.getElementById('contact');


home.addEventListener('click', (e) =>{
	page.change(new homeState);
});
about.addEventListener('click', (e) =>{
	page.change(new aboutState);
});
contact.addEventListener('click', (e) =>{
	page.change(new contactState);
});