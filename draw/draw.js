// Create a canvas that extends the entire screen
// and it will draw right over the other html elements, like buttons, etc
var canvas = document.createElement("canvas");
canvas.setAttribute("width", window.innerWidth);
canvas.setAttribute("height", window.innerHeight);
canvas.setAttribute("style", "position: absolute; x:0; y:0;");
document.body.appendChild(canvas);

//Then you can draw a point at (10,10) like this:

var ctx = canvas.getContext("2d");



    var img = new Image();
    img.src = "pointer.png";
    img.width = 20;
    img.height = 20;
    var y = 50;
    var	coordinates, oldX=null, oldY=null;


    // ctx.drawImage(img, 200, y,img.width, img.height);

    coordinates = getCoordinates();

    coordinates.forEach(function(coordinate){

    	setTimeout(drawPointer(coordinate[0],coordinate[1]), 1000);

    	// console.log(coordinate); 
    });


    function drawPointer(x,y){

    	if (oldX != null) {
    		ctx.clearRect(oldX, oldY, img.width, img.height);
 		}
 		
    	ctx.drawImage(img, 200, y,img.width, img.height);
    	oldX = x;
    	oldY = y;
    }



    function getCoordinates(){
			
			let coordinates;

			// Check if  any item in LS
			if (localStorage.getItem('coordinates')=== null) {

				coordinates = [];

				

			}else{
				coordinates = JSON.parse(localStorage.getItem('coordinates'));

				return coordinates;
			}

		}